package alfabank.louco.com.alfanews.mvp.presenter;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import alfabank.louco.com.alfanews.mvp.model.db.NewsDatabase;
import alfabank.louco.com.alfanews.mvp.view.NewsListView;

@InjectViewState
public class NewsListPresenter extends MvpPresenter<NewsListView> {

    private NewsDatabase newsDatabase;
    private ContentNewsDao newsDao;

    private static boolean favoriteList = false;

    private LiveData<List<Item>> listLiveData;

    public NewsListPresenter() { }

    public void getNewsList(Activity activity) {
        newsDatabase = NewsDatabase.getInstanceNews(activity);
        newsDao = newsDatabase.getContentNewsDao();
        checkTypeList((LifecycleOwner) activity);
    }

    public void floatButtonAction(Activity activity){
        favoriteList = !favoriteList;
        checkTypeList((LifecycleOwner) activity);
    }

    private void checkTypeList(LifecycleOwner activity) {
        if(listLiveData != null) {
            listLiveData.removeObservers(activity);
        }
        listLiveData = getTypeListNews(newsDao);

        setLiveDataSubscribe(activity);
        getViewState().setFloatButtonIcon(favoriteList);
    }


    private void setLiveDataSubscribe(LifecycleOwner activity) {
        listLiveData.observe(activity, items ->
            getViewState().reloadNewsList(items)
        );
    }

    public void onClickNews(int position) {
        getViewState().showNews(position);
    }

    public static LiveData<List<Item>> getTypeListNews(ContentNewsDao newsDao){
        if(favoriteList){
            return newsDao.getAllNewsItemFavorite();
        }else{
            return newsDao.getAllNewsItem();

        }
    }

}
