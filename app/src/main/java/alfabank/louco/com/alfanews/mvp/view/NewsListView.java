package alfabank.louco.com.alfanews.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;

public interface NewsListView extends MvpView{

    /**
     * @param position - С какой позиции отображать новости
     *                           в новой активности
     */
    @StateStrategyType(value = SkipStrategy.class)
    void showNews(int position);

    /**
     * @param itemList - вывести список новостей
     */
    @StateStrategyType(value = AddToEndSingleStrategy.class)
    void reloadNewsList(List<Item> itemList);


    void setFloatButtonIcon(Boolean typeList);
}
