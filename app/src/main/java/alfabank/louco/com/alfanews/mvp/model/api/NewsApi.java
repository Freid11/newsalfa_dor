package alfabank.louco.com.alfanews.mvp.model.api;

import alfabank.louco.com.alfanews.mvp.model.Object.Rss;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {

    @GET("_rss.html")
    Observable<Rss> getNews(@Query("subtype") Integer subtype, @Query("category") Integer category, @Query("city") Integer city );
}
