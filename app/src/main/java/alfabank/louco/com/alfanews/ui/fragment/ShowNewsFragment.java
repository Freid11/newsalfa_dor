package alfabank.louco.com.alfanews.ui.fragment;

import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import alfabank.louco.com.alfanews.R;
import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.mvp.model.db.ContentNewsDao;
import alfabank.louco.com.alfanews.mvp.model.db.NewsDatabase;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowNewsFragment extends Fragment {

    private static final String ITEM_KEY = "item_key";

    private Item itemNews;
    private NewsDatabase newsDatabase;
    private ContentNewsDao contentNewsDao;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.wv_news)
    WebView webNews;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            itemNews = savedInstanceState.getParcelable(ITEM_KEY);
        }
        setHasOptionsMenu(true);
    }

    public static Fragment instance(Item item) {
        ShowNewsFragment showNewsFragment = new ShowNewsFragment();
        showNewsFragment.setItemNews(item);
        return showNewsFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if (container != null) {
            inflater = LayoutInflater.from(container.getContext());
        }
        View view = inflater.inflate(R.layout.fragment_show_news, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(ITEM_KEY, itemNews);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        newsDatabase = NewsDatabase.getInstanceNews(getActivity().getBaseContext());
        contentNewsDao = newsDatabase.getContentNewsDao();

        switchIconFab();
        isActiveNetwork();

        webNews.post(() -> webNews.loadUrl(itemNews.getLink()));

        fab.setOnClickListener(view1 -> {
            itemNews.setFavorite(!itemNews.getFavorite());
            contentNewsDao.update(itemNews);
            switchIconFab();
        });
    }

    private void isActiveNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);

        if (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
            webNews.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        } else {
            webNews.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
    }

    private void switchIconFab() {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if (itemNews.getFavorite()) {
            fab.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_favorite));
        } else {
            fab.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_favorite_border));
        }
    }

    public void setItemNews(Item itemNews) {
        this.itemNews = itemNews;
    }
}
