package alfabank.louco.com.alfanews.ui.adapter;

import android.database.DataSetObserver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import alfabank.louco.com.alfanews.ui.fragment.ShowNewsFragment;

public class VpAdapterNews extends FragmentPagerAdapter {

    private List<Item> itemList;

    public VpAdapterNews(FragmentManager fm, List<Item> itemList) {
        super(fm);
        this.itemList = itemList;
    }

    @Override
    public Fragment getItem(int position) {
        return ShowNewsFragment.instance(itemList.get(position));
    }

    @Override
    public int getCount() {
        return itemList.size();
    }


    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void registerDataSetObserver(@NonNull DataSetObserver observer) {
        super.registerDataSetObserver(observer);
    }


}
