package alfabank.louco.com.alfanews.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;

public interface ShowNewsActivityView extends MvpView {

    @StateStrategyType(value = SkipStrategy.class)
    void ShowNews(List<Item> itemList, int position);
}
