package alfabank.louco.com.alfanews.mvp.model.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;

@Dao
public interface NewsDao {

    @Query("SELECT * FROM item WHERE favorite = 0")
    LiveData<List<Item>> getAllNewsItem();

    @Query("SELECT * FROM item WHERE favorite = 1")
    LiveData<List<Item>> getAllNewsItemFavorite();

    @Query("DELETE FROM item WHERE favorite = 0")
    void clearBase();

    @Insert
    void insert(Item item);

    @Insert
    void insertList(List<Item> item);

    @Update
    void update(Item item);

    @Delete
    void delete(Item item);
}
