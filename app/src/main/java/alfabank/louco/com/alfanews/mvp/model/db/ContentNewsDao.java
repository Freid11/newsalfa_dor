package alfabank.louco.com.alfanews.mvp.model.db;

import android.annotation.SuppressLint;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import alfabank.louco.com.alfanews.mvp.model.Object.Item;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class ContentNewsDao implements NewsDao {
    private static final String TAG = "ContentNewsDao";
    private NewsDao newsDao;

    ContentNewsDao(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    @Override
    public LiveData<List<Item>> getAllNewsItem() {
        return newsDao.getAllNewsItem();
    }

    @Override
    public LiveData<List<Item>> getAllNewsItemFavorite() {
        return newsDao.getAllNewsItemFavorite();
    }

    @SuppressLint("CheckResult")
    @Override
    public void clearBase() {
        Log.d(TAG,"clearBase");
        Single.just(newsDao)
                .observeOn(Schedulers.io())
                .subscribe(NewsDao::clearBase);
    }

    @SuppressLint("CheckResult")
    @Override
    public void insert(Item item) {
        Log.d(TAG,"insert");
        Single.just(item)
                .observeOn(Schedulers.io())
                .subscribe(newsDao::insert);
    }

    @SuppressLint("CheckResult")
    @Override
    public void insertList(List<Item> items) {
        Log.d(TAG,"insertList");
        Single.just(items)
                .observeOn(Schedulers.io())
                .subscribe(newsDao::insertList);
    }

    @SuppressLint("CheckResult")
    @Override
    public void update(Item item) {
        Log.d(TAG,"update");
        Single.just(item)
                .observeOn(Schedulers.io())
                .subscribe(newsDao::update);
    }

    @SuppressLint("CheckResult")
    @Override
    public void delete(Item item) {
        Log.d(TAG,"delete");
        Single.just(item)
                .observeOn(Schedulers.io())
                .subscribe(newsDao::delete);
    }
}
